import logo from './logo.svg';
import './App.css';
import React from 'react';
import { withAuthenticator, AmplifySignOut } from '@aws-amplify/ui-react'

function App() {
  return (
    <div className="App">
      <header className="App-header">
       <h1> Hello Lifesaver</h1>
      </header>
      <AmplifySignOut/>
    </div>
  );
}

export default withAuthenticator(App);
